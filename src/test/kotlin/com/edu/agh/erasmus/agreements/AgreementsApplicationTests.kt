package com.edu.agh.erasmus.agreements

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@ActiveProfiles("integration")
@RunWith(SpringRunner::class)
@SpringBootTest
class AgreementsApplicationTests {

	@Test
	fun contextLoads() {
	}

}
