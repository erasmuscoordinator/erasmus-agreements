package com.edu.agh.erasmus.agreements.bulk

import com.edu.agh.erasmus.agreements.model.Agreement
import com.edu.agh.erasmus.agreements.model.AgreementRepository
import com.edu.agh.erasmus.agreements.model.Agreements
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BulkAgreementService @Autowired constructor(
        private val bulkAgreementRepository: AgreementRepository
) {
    fun bulkAddAgreements(bulkAgreements: Agreements): List<Agreement> {
        return bulkAgreementRepository.insert(bulkAgreements.agreements)
    }
}