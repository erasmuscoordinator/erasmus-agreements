package com.edu.agh.erasmus.agreements.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import static org.springframework.http.HttpMethod.GET;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final String secret;

    public WebSecurity(@Value("${jwt.secret}") String secret) {
        this.secret = secret;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeRequests()
                .antMatchers("/v2/api-docs", "/configuration/ui", "/swagger-resources/**",
                        "/configuration/security", "/swagger-ui.html", "/webjars/**").permitAll()
                .antMatchers(GET, "/agreements/search/**").authenticated()
                .antMatchers(GET, "/agreements/**").authenticated()
                .anyRequest().hasAuthority("ADMINISTRATOR")
                .and()
                .addFilter(new JwtAuthorizationFilter(authenticationManager(), secret))
                // this disables session creation on Spring Security
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }
}
