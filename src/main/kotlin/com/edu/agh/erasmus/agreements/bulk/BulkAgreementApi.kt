package com.edu.agh.erasmus.agreements.bulk

import com.edu.agh.erasmus.agreements.model.Agreement
import com.edu.agh.erasmus.agreements.model.Agreements
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/agreements/bulk")
class BulkAgreementApi @Autowired constructor(
        private val bulkAgreementService: BulkAgreementService
){
    @PostMapping
    @ApiOperation(value = "Creates many agreements at once")
    fun addOffer(@RequestBody agreements: Agreements): ResponseEntity<List<Agreement>> {
        return ResponseEntity(bulkAgreementService.bulkAddAgreements(agreements), HttpStatus.CREATED);

    }
}