package com.edu.agh.erasmus.agreements.security;

class SecurityProperties {
    static final String TOKEN_PREFIX = "Bearer ";
    static final String HEADER_STRING = "Authorization";
}
