package com.edu.agh.erasmus.agreements

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
open class AgreementsApplication

fun main(args: Array<String>) {
    SpringApplication.run(AgreementsApplication::class.java, *args)
}
