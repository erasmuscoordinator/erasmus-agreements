package com.edu.agh.erasmus.agreements.config

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration

import springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@Import(SpringDataRestConfiguration::class)
open class SwaggerConfig {

    @Bean
    open fun swaggerApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .groupName("api")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/agreements.*"))
                .build()
    }

    private fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title("Erasmus Agreements API Documentation")
                .description("Spring REST Documentation with Swagger for AGH Erasmus Agreements Application")
                .version("0.1")
                .build()
    }
}
