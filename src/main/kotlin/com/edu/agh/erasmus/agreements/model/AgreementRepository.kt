package com.edu.agh.erasmus.agreements.model

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.data.rest.core.annotation.RestResource

@RepositoryRestResource(collectionResourceRel = "agreements", path = "agreements")
interface AgreementRepository : MongoRepository<Agreement, String> {

    @RestResource(path = "countries") fun findByCountryIgnoreCase(@Param("country") country: String, p: Pageable): Page<Agreement>
    @RestResource(path = "codes") fun findByCode(@Param("code") code: String, p: Pageable): Page<Agreement>
    @RestResource(path = "universityNames") fun findByUniversityNameIgnoreCase(@Param("universityName") universityName: String, p: Pageable): Page<Agreement>
    @RestResource(path = "coordinators") fun findByCoordinatorContainingIgnoreCase(@Param("coordinator") coordinator: String, p: Pageable): Page<Agreement>
    @RestResource(path = "departments") fun findByDepartmentIgnoreCase(@Param("department") department: String, p: Pageable): Page<Agreement>
    @RestResource(path = "startYears") fun findByStartYear(@Param("startYear") startYear: String, p: Pageable): Page<Agreement>
    @RestResource(path = "endYears") fun findByEndYear(@Param("endYear") endYear: String, p: Pageable): Page<Agreement>
    @RestResource(path = "durations") fun findByDurationIgnoreCase(@Param("duration") duration: String, p: Pageable): Page<Agreement>
    @RestResource(path = "departmentCoordinators") fun findByDepartmentCoordinatorIgnoreCase(@Param("departmentCoordinator") departmentCoordinator: String, p: Pageable): Page<Agreement>
    @RestResource(path = "vacancies") fun findByVacancies(@Param("vacancies") vacancies: String, p: Pageable): Page<Agreement>

    @RestResource(path = "departmentAndUniversityName")
    fun findByDepartmentIgnoreCaseAndUniversityNameIgnoreCase(@Param("department") department: String,
                                                              @Param("universityName") universityName:
                                                              String, p: Pageable): Page<Agreement>

    @RestResource(path = "departmentAndCountry")
    fun findByDepartmentIgnoreCaseAndCountryIgnoreCase(@Param("department") department: String,
                                                       @Param("country") country: String,
                                                       p: Pageable): Page<Agreement>

    @RestResource(path = "departmentAndCoordinator")
    fun findByDepartmentIgnoreCaseAndCoordinatorIgnoreCase(@Param("department") department: String,
                                                           @Param("coordinator") coordinator: String,
                                                           p: Pageable): Page<Agreement>
}