package com.edu.agh.erasmus.agreements.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonProperty.Access.READ_ONLY
import org.springframework.data.annotation.Id

data class Agreement(
        @Id @set:JsonIgnore @get:JsonProperty(access = READ_ONLY)
        var id: String? = null,
        val country: String? = null,
        val code: String? = null,
        val universityName: String? = null,
        val startYear: String? = null,
        val endYear: String? = null,
        val department: String? = null,
        val coordinator: String? = null,
        val departmentCoordinator: String? = null,
        val duration: String? = null,
        val vacancies: String? = null
)

data class Agreements(
        val agreements: Set<Agreement> = emptySet()
)